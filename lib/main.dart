import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:module3navigation/generated/i18n.dart';

import 'Module5_Networking/PhotoLogoPage.dart';

/*

Based on a previous task ( 2 screens with navigation forward and back):

First screen displaying some default content and button "GO TO 2nd SCREEN"
By pressing this button we're opening 2nd screen and requesting some info from https://jsonplaceholder.typicode.com or any other remote source
After it's been fetched - displaying those data on the screen screen
If we pressed to any of these data items - go back to 1st screen and display chosen data.

 */

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  final i18n = I18n.delegate;

  @override
  void initState() {
    super.initState();
    I18n.onLocaleChanged = onLocaleChange;
  }

  void onLocaleChange(Locale locale) {
    setState(() {
      I18n.locale = locale;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      localizationsDelegates: [
        I18n.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate
      ],
      supportedLocales: i18n.supportedLocales,
      onGenerateTitle: (BuildContext context) => I18n.of(context).titlesMain,
      home: PhotoLogoPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}
