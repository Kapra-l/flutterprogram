import 'dart:async';
import 'dart:convert';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:module3navigation/generated/i18n.dart';

class PhotoItem {
  final int albumId;
  final int id;
  final String title;
  final String url;
  final String thumbnailUrl;

  PhotoItem({this.albumId, this.id, this.title, this.url, this.thumbnailUrl});

  factory PhotoItem.fromJson(Map<String, dynamic> json) {
    return PhotoItem(
      albumId: json['albumId'],
      id: json['id'],
      title: json['title'],
      url: json['url'],
      thumbnailUrl: json['thumbnailUrl'],
    );
  }
}

Future<List<PhotoItem>> getPhotoItems(BuildContext context) async {
  final response = await http.get("https://jsonplaceholder.typicode.com/albums/1/photos");

  if (response.statusCode == 200) {
    var listItems = (json.decode(response.body) as List).map((data) => PhotoItem.fromJson(data)).toList();
    return listItems;
  }
  else {
    throw new Exception(I18n.of(context).exceptionsLoad_album_failed);
  }
}