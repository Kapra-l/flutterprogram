import 'dart:io';

import 'package:flutter/material.dart';
import 'package:module3navigation/Module5_Networking/PhotoItem.dart';
import 'package:module3navigation/Module5_Networking/PhotoItemListPage.dart';
import 'package:module3navigation/generated/i18n.dart';

class PhotoLogoPage extends StatefulWidget {

  @override
  PhotoLogoState createState() => PhotoLogoState();
}

class PhotoLogoState extends State<PhotoLogoPage> {

  PhotoItem selectedPhotoItem = null;
  bool isEnglish = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(I18n.of(context).titlesFirst_screen),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 16.0),
            child: GestureDetector(
              child: Icon(Icons.language),
              onTap: () {
                var newLocale = isEnglish ? I18n.delegate.supportedLocales[1] : I18n.delegate.supportedLocales[0];

                I18n.onLocaleChanged(newLocale);
                isEnglish = !isEnglish;
              },
            )
          )
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        label: new Text(I18n.of(context).buttonsGo_to_second_screen),
        backgroundColor: Colors.blue,
        onPressed: () async {
          var result = await Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => PhotoItemListPage()));

          if (result != null)
          {
            setState(() {
              selectedPhotoItem = result;
            });
          }
        },
      ),
      body: ListView(
        padding: EdgeInsets.only(left: 32, right: 32, bottom: 32),
        children: <Widget>[
          headerSection(),
        ],
      )
    );
  }

  Widget headerSection() => Container(
    child: Row(
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              selectedPhotoItem != null ? Center(child: Image.network(selectedPhotoItem.url)) : Icon(Icons.image),
              Container(
                padding: EdgeInsets.only(top: 8, bottom: 8),
                child: Text(
                  selectedPhotoItem != null ? selectedPhotoItem.title : I18n.of(context).labelsPhoto_title,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                child: Text(
                  selectedPhotoItem != null ? selectedPhotoItem.url : I18n.of(context).labelsPhoto_url,
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}
