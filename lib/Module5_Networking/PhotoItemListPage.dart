import 'package:flutter/material.dart';
import 'package:module3navigation/Module5_Networking/PhotoItem.dart';
import 'package:module3navigation/generated/i18n.dart';

import 'PhotoLogoPage.dart';

class PhotoItemListPage extends StatefulWidget {

  @override
  PhotoItemsState createState() => PhotoItemsState();
}

class PhotoItemsState extends State<PhotoItemListPage> {

  Future<List<PhotoItem>> futurePhotoItems;

  @override
  void initState() {
    super.initState();
    futurePhotoItems = getPhotoItems(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(I18n.of(context).titlesSecond_screen),
        ),
        body: Center(
            child: FutureBuilder<List<PhotoItem>>(
              future: futurePhotoItems,
              builder: (context, items) {
                if (items.hasData) {
                  return ListView.builder(
                      itemCount: items.data == null ? 0 : items.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return _photoItemTile(context, items.data[index]);
                      }
                  );
                }
                else if (items.hasError) {
                  return Text("${items.error}");
                }

                return CircularProgressIndicator();
              },
            )
        )
    );
  }
}

ListTile _photoItemTile(BuildContext context, PhotoItem photoItem) => ListTile(
  leading: Image.network(photoItem.thumbnailUrl),
  title: Text(photoItem.title,
      overflow: TextOverflow.ellipsis,
      style: TextStyle(
          fontSize: 18.0,
          color: Colors.black87)),
  subtitle: Text(photoItem.url),
  onTap: () => Navigator.pop(context, photoItem),
);